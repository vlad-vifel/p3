window.addEventListener('DOMContentLoaded', () => { 

    document.querySelector('.works__nav').addEventListener('click', (event) => {
        if(event.target.classList.contains('nav-works__button')) {

            document.querySelector('.nav-works__button--active').classList.remove('nav-works__button--active')

            event.target.classList.add('nav-works__button--active')

            const filterValue = event.target.getAttribute('data-filter')


            document.querySelectorAll('.gallery__photo').forEach((photo) => {


                if (filterValue === 'all') {
                    photo.classList.remove('gallery__photo--shown')
                    photo.classList.remove('gallery__photo--hidden')

                    if(photo.classList.contains('gallery__photo_small')) {
                        photo.classList.remove('gallery__photo_big')
                    }

                } else if (photo.getAttribute('alt') === filterValue) {
                    photo.classList.remove('gallery__photo--hidden')
                    photo.classList.add('gallery__photo--shown')
                    
                    console.log('!!!')

                    if(photo.classList.contains('gallery__photo_small')) {
                        console.log('!!!!!')
                        photo.classList.add('gallery__photo_big')
                    }

                } else {
                    photo.classList.remove('gallery__photo--shown')
                    photo.classList.add('gallery__photo--hidden')

                    if(photo.classList.contains('gallery__photo_small')) {
                        photo.classList.remove('gallery__photo_big')
                    }
                }



            })

            

        }
    })

    document.querySelector('.cost').addEventListener('click', (event) => {

        const workType = document.querySelector('#select__work-type')
        const workSize = document.querySelector('#select__work-size')
        const checkbox = document.querySelector('.checkbox')
        const button = document.querySelector('.options-cost__button')

        const blockResult = document.querySelector('.cost__result')


        if(workType.querySelector('option:checked').getAttribute('value')==='s1') {
            workSize.setAttribute('disabled','')
        } else {
            workSize.removeAttribute('disabled')
        }

        if(workType.querySelector('option:checked').getAttribute('value')==='s1' || workSize.querySelector('option:checked').getAttribute('value')==='s1' || !checkbox.checked) {
            button.setAttribute('disabled', '')
        } else {
            button.removeAttribute('disabled')
        }


        if(event.target.classList.contains('options-cost__button') && !button.hasAttribute('disabled')) {
            blockResult.classList.remove('cost__result--hidden')
        }
    })


    document.querySelectorAll('.block-faq__container').forEach((el) => {
        el.addEventListener('click', () => {

            let container = el;
            let content = el.nextElementSibling

            if(content.classList.contains('block-faq__content--current')) {
                document.querySelectorAll('.block-faq__container').forEach((el) => { el.classList.remove('block-faq__container--current') })
                document.querySelectorAll('.block-faq__content').forEach((el) => { el.classList.remove('block-faq__content--current') })
            } else {
                document.querySelectorAll('.block-faq__container').forEach((el) => { el.classList.remove('block-faq__container--current') })
                document.querySelectorAll('.block-faq__content').forEach((el) => { el.classList.remove('block-faq__content--current') })

                content.classList.add('block-faq__content--current')
                container.classList.add('block-faq__container--current')
            }
        })

    })

})